# Nombre: Julen San Tirso Hernández
# Entrega: Estadistica descriptiva (Tercera entrega)
# Fecha:27/10/2017
#---------------------------------------------------------------------------
rm(list = ls());cat("\014")
setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
getwd()
library(ggplot2)
library(GGally)
library(reshape2)

# (incluya aquí cualquier librería adicional)
#---------------------------------------------------------------------------

# Paso 01: Lea los datos del fichero “crx.csv” y los guarde en un data.frame

datos_crx = read.csv("crx.csv")

# Paso 02: En este caso, los valores perdidos se denotan como ‘?’ -> Convertirlos todos a NA

datos_crx[datos_crx=="?"] <- NA

# Paso 03: Calcule la matriz de correlación y muestre los 5 pares de atributos más correlacionados
# Sin contar una variable consigo misma, claro. Y sin contar parejas [cor(a,b)==cor(b,a)]
# Tenga en cuenta medir las correlaciones en valor absoluto! [correlación de -0.99 es más fuerte que una de 0.9]
# * Hay varios caminos para hacer ésto, se puede programar con FORs, o hacer uso de algunas
# * funciones de R, como upper.tri(), melt() [paquete reshape2] 

# Compruebo transformo la matriz a numerica para diferenciar las columnas numericas de las no numericas, para a continuacion separar los datos en las 
# matrices correspondientes (matriz_numerica y matriz_caracter).

matriz_temp= apply(datos_crx,2,function(x) as.numeric(x))
matriz_numerica = matriz_temp[,-which(apply(matriz_temp,2,function(x) (all(is.na(x)))))]
matriz_caracter = datos_crx[,-which(apply(matriz_temp,2,function(x) !(all(is.na(x)))))]

# Con la funcion cor creo la matriz de correlacion y con abs su valor absoluto (pairwise.complete.obs lo que hace es tener en cuenta los valores de NA)

matriz_correlacion= abs(cor(matriz_numerica, use = "pairwise.complete.obs"))

# lower.tri me elimina el triangulo de abajo de la matriz para que no salgan repetidos los valores entre mismas columnas con diferente orden [cor(a,b)==cor(b,a)]
# Sustituye los valores repetidos por NA

matriz_correlacion[lower.tri(matriz_correlacion)] <- NA

# Melt me da la matriz de correlacion en una lista y na.rm= TRUE hace que los valores NA no se tengan en cuenta

lista = melt(matriz_correlacion, na.rm = TRUE)

# Como todavia tengo los valore de una columna con su propia columna, para eliminarlos elimino las filas que tengan menos de 3 valores unicos
# esto lo hago por que en la matriz de la lista los valores que se hagan consigo mismos saldran asi [A1, A1, 1] por lo tanto, solo son 2 valores unicos, sino serian 3 [A1,A2,0.55]

lista = lista[-which(apply(lista,1, function(x) length(unique(x)) <3)),] 
lista_ordenada = lista[order(-lista$value)[1:5],] 

# Paso 04: Automáticamente, para cada atributo, muestre por pantalla:
#     Si es numérico: la media, mediana, Q1, Q3, desviación estándar, valores mínimo y máximo, número de valores perdidos
#     Si no es numérico: número de valores únicos y porcentaje de apariciones de cada valor único

# En este caso podria hacer los applys dentro de un for o una funcion, pero como es para su visualizacion, considero que es mucho mejor poder ir visualizandolos
# uno a uno y de manera independiente. Uso el na.rm otra vez para que me calcule bien las funciones sin NA's

# Numericas
apply(matriz_numerica,2, mean,na.rm = TRUE)
apply(matriz_numerica,2, median,na.rm = TRUE)
apply(matriz_numerica,2, function(x) quantile(x,.25,na.rm = TRUE))
apply(matriz_numerica,2, function(x) quantile(x,.75,na.rm = TRUE))
apply(matriz_numerica,2, sd,na.rm = TRUE)
apply(matriz_numerica,2, max,na.rm = TRUE)
apply(matriz_numerica,2, min,na.rm = TRUE)
apply(matriz_numerica,2, function(x) sum(is.na(x)))

# No numericas
apply(matriz_caracter,2, function(x) (table(x)/nrow(matriz_caracter))*100)

# Paso 05: Agregue los datos por los valores de los atributos A1 y A4 usando la función media
# * Tenga en cuenta que la agregación sólo se podrá hacer para las columnas numéricas

mean_data = aggregate(matriz_numerica, by=list(A1 = matriz_caracter$A1, A4 = matriz_caracter$A4), FUN=mean, na.rm=TRUE)

# Paso 06: Haga uso de la función ggplot [librería ggplot2] y dibuje:
#      El boxplot para cada variable numérica
#      El barplot para cada variable no-numérica
#      El scatterplot (función ggpairs) de todas las variables
#      Usando (en todas) la columna “Class” para definir el color

color <- table(matriz_caracter$Class)
apply(matriz_numerica,2, function(x) boxplot(x,color,main=nrow(x),col=c("red","green")))
sapply(matriz_caracter,function(x) barplot(color,main=colnames(x),col=c("red","green"),legend = rownames(color)))
 
# Uno las dos matrices para dibujar el scatterplot

matriz_final = cbind(matriz_numerica, matriz_caracter)
grafico <- ggpairs(
  matriz_final,
  mapping = aes(color = Class),
  columns=1:NCOL(matriz_final),
  title = "Scatterplot"
)
grafico

