# Nombre:Julen San Tirso Hernández 
# Entrega: Particion y overffiting (Cuarta entrega)
# Fecha: 01/10/2017
#---------------------------------------------------------------------------
rm(list = ls());cat("\014")
setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
getwd()
library(rpart)
library(caret)
library(ggplot2)
# (incluya aquí cualquier librería adicional)
#---------------------------------------------------------------------------

# Paso 01: Lea los datos del fichero “churn.csv” y los guarde en un data.frame

datos_churn = read.csv("churn.csv")

# Paso 02: Elimine aquellas columnas no numéricas que tengan más de 1000 
# niveles diferentes

datos_churn2 =datos_churn[,-which(sapply(datos_churn,function(x) !is.numeric(x) && length(unique(x))>1000))]

# Paso 03: Divida los datos para hacer 10-fold cross validation, utilizando
# la función apropiada del paquete caret

# Elijo el numero de grupos o folders y los creo.

fold_number = 10
churn_folds = createFolds(datos_churn2$Churn, k = fold_number, list = TRUE, returnTrain = TRUE)

# Paso 04: Para cada uno del os folds:
#  - Utilice la función “rpart” para construir un árbol de decisión con los datos training
#  - Utilice la función “predict” para probar el árbol construido en el paso anterior sobre los datos de training y test
#  - Almacene en un vector los valores:
#  - minsplit, maxdepth, Porcentaje de fallos promedio (de las 10 ejecuciones) tanto en training y test

# Creo una funcion que calcula el error con una matriz de confusion

calcular_error = function(real, prediction){
  confMat = table(real, prediction)
  precision = sum(diag(confMat))/sum(confMat)
  return(1-precision)
}

# Creo una funcion que procesa los folds con los valores de "min_split" y "max_depth"

processFold = function(fold, min, maxd){
 
  # Para ello defino la formula, divido en train y test y tras calcular el devuelvo guardo los resultados 
  
  formula = as.formula(Churn~.)
  
  datos_churn_train = datos_churn2[unlist(fold),]
  datos_churn_test = datos_churn2[-unlist(fold),]

  control = rpart.control(minsplit=min, maxdepth=maxd)
  modelo = rpart(Churn~., method="class", data=datos_churn_train, control = control)

  prediccion_train = predict(modelo, datos_churn_train, type = "class")
  prediccion_test = predict(modelo, datos_churn_test, type = "class")
  
  train_error = calcular_error(datos_churn_train$Churn, prediccion_train)
  test_error = calcular_error(datos_churn_test$Churn, prediccion_test)
  return( c(min, maxd, train_error, test_error) )
}

# Proceso todos los folds creados, anidando las funciones creadas anteriormente

matriz_resultados = sapply(churn_folds, function(x) processFold(x, 20, 30))
print(matriz_resultados)

# Transpongo la matriz para poder ordenarla mejor

matriz_resultados = t(matriz_resultados)
colnames(matriz_resultados) = c("min_split", "max_depth", "avg_train_error", "avg_test_error")

# Paso 05: Construya y pruebe árboles de decisión
# Itere sobre maxdepth = {1...30} y minsplit = {1,10,20,...500}
# Almacene en minsplit, maxdepth, error promedio en training y test
# Obtenga los valores “óptimos” de los parámetros para este problema
# (Hay otros parámetros que se pueden optimizar, pero sólo usaremos estos 2)

# Defino los valores que se nos piden para minsplit y maxdepth

valores_maxdepth = c(1:30)
valore_minsplit = seq(0, 500, 10)

# Divido en train y test

datos_ind= createDataPartition(datos_churn2[,ncol(datos_churn2)], times = 1, p = 0.8, list = FALSE)
datos_churn2_train = datos_churn2[datos_ind,]
datos_churn2_test = datos_churn2[-datos_ind,]

# Creo una funcion que genere los arboles de decision 

arbolesDecision =  function(x, min, depth){
  
  # Defino la formula y entreno el modelo
  
  formula = as.formula(Churn~.)
  control = rpart.control(minsplit=min, maxdepth=depth)
  modelo = rpart(Churn~., method="class", data=datos_churn2_train, control=control)
  prediccion_test = predict(modelo, datos_churn2_test, type = "class")
  
  # Calculo el error y devuelvo los datos
  
  test_error = calcular_error(datos_churn2_test$Churn, prediccion_test)
  return( c(depth, min, test_error) )
}

# Creo una funcion para iterar sobre todos los valores que nos han pedido y sus conbinaciones

iterarSobreMinVal = function(depth, min){
  print( paste("Progreso: maxdepth: ", depth, " minsplit: ", min) )
  return( arbolesDecision(datos_churn2_train, min, depth) )
}

# Creo una funcion parar iterar sobre los valores de depth. Y la enlazo con la anterior

iterarSobreMaxdepth = function(depth){
  temp=sapply(valore_minsplit, function(minval) iterarSobreMinVal(depth, minval))
  temp=t(temp)
  print(temp)
  return(temp)
}

# anido dos funciones sapply y lapply para iterar sobre las columnas y las filas (una de ellas esta en la funcion iterarSobreMaxdepth)

temp=lapply(valores_maxdepth, iterarSobreMaxdepth)

# Busco la mejor configuracion posible, para eso me recorro la matriz temporal con los valores definitivos y miro cual es el menor error

minimo_error = 1
mejor_config = 0
filas_procesadas = matrix(NA, 0, 4)
for(i in 1:length(temp)){
  actual=temp[i]
  actual=as.data.frame(actual)
  filas_procesadas=rbind(filas_procesadas, actual)
  min_val = min(actual[3])
  if(min_val < minimo_error){
    minimo_error=min_val
    mejor_config = i
  }
}

# Identifico si la hay y en ese caso, cual seria.

if(mejor_config > 0){
  print( paste("Indice con mejor configuracion: ", mejor_config) )
  print("Mejores valores:")
  frame = as.data.frame(temp[mejor_config])
  colnames(frame) = c("min_split", "max_depth", "error");
  best_idx = which( frame$error == min(frame$error))
  print( paste("Conjunto de mejores configuraciones: ", length(best_idx) ) )
  print(frame[best_idx,])
} else {
  print("No se ha encontrado configuracion optima")
}

# Paso 06: Utilice la librería ggplot2 para hacer para hacer un gráfico de "azulejos":
#  - Eje X -> Valor de maxdepth
#  - Eje Y -> Valor de minsplit
#  - Color -> Porcentaje de error (promedio) en test
# * Puede utilizar geom_tile() o geom_raster()

ggplot(
  filas_procesadas, 
  aes(filas_procesadas$X1, filas_procesadas$X2, fill = filas_procesadas$X3)
) + geom_raster(hjust = 0, vjust = 0) + ggtitle("Grafico de azulejos")+ xlab("Depth") + ylab("Min_split") + labs(fill="Error rate")

